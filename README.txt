CONTRIBUTIONS:
Both of us worked together on the project in Annenberg lab, and so any
code that we added to the existing files was a mutual contribution.
Specifics are described below. 

DESCRIPTION:
[Part 1]
	* At the very start of the program we created the board object, and
	  made appropriate changes to the respective .h file as well.
	* For the simplest version of the player, we tried to have the
	  program iterate through all spots of the board and test whether or
	  not that specific spot could be a valid move for the program's
	  respective color of stone. To do this, we used double-nested for
	  loops that went through all horizontal and all vertical positions,
	  respectively, on the board.  These would only run though if the
	  given hasMoves function returned true when we used this function
	  on our board object.  If there were no moves possible, we returned
	  NULL, in theory passing our turn and allowing the game to continue.
	  At one point, we tried to add an array to which we would add all
	  possible moves we found in one particular board arrangement.
	  From this array, we wanted to then choose and return/play a random
	  move.  We could not, however, get this to work, and so we ommitted
	  this array-process from our program of part 1.
	* Our part 1 still did not run properly when we turned this file in;
	  however, this version of the program returns the first of the possible
	  valid moves found immediately.  This will not be ideal, but it
	  should still be able, in theory, to play a valid game of Othello.
	  
[Part 2]
	* Before starting the assignment for this week (part 2), we had to
	  fix the problems with the assignment we turned in for the first
	  week.  To do this, we had to define the opponent's side, not only
	  our own, which was the main mistake we made for part 1.
	* We also attempted to made an array to hold all possible valid moves,
	  and had the program play a random choice from this array, rather
	  than returning the first possible found valid move; however, we
	  were not able to find the error in the code involving the array,
	  and so our program once again doesn't run properly.
	* We were not able to implement alpha-beta pruning (as most of our
	  time was spent debugging code from Part 1 due to language details);
	  however, we understand the main concepts behind this method in AI
	  programming:
	  i.e. taking the minimum score that our side is sure to gain by a
	  specific move versus the maximum score the opponent gains from that
	  same move for all of the possible moves along a tree, we can then
	  move along this tree like we did in assignment 5 with MSTs to find
	  the optimal next move out of all valid moves in one play.
	  
