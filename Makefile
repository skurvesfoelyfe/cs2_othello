CC          = g++
CFLAGS      = -Wall -ansi -pedantic -ggdb
OBJS        = player.o wrapper.o board.o
skurvesfoelyfe  = player

all: $(skurvesfoelyfe) testgame
	
$(skurvesfoelyfe): $(OBJS)
	$(CC) -o $@ $^
        
TestGame: testgame.o
	$(CC) -o $@ $^
        
%.o: %.cpp
	$(CC) -c $(CFLAGS) -x c++ $< -o $@
	
java:
	make -C java/

cleanjava:
	make -C java/ clean

clean:
	rm -f *.o $(skurvesfoelyfe) testgame		
	
.PHONY: java
