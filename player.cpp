#include "player.h"
#include <stdlib.h>
using namespace std;

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     
    board = new Board(); // Declaring board object referenced in .h file.
    mySide = side;
     if(mySide == WHITE)
     {
		 opponent = BLACK;
	 }
	 else
	 {
		 opponent = WHITE;
	 }

}

/*
 * Destructor for the player.
 */
Player::~Player() {
	delete board;
}
/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's move before calculating your own move
     */ 
    
    // The total number of valid moves in one play (we defined the
    //  function in our board files.
    int numMoves = board->countMoves(this->mySide);
    
    // Array to hold all of the possible valid moves:
    Move **allMoves;
    allMoves = new Move*[numMoves];
    
    // Processing opponent move:
    if (this->mySide == BLACK){
		board->doMove(opponentsMove, WHITE);
	}
	else{
		board->doMove(opponentsMove, BLACK);
	}
	
	int x = 0;
    // Calculating and returning personal move:
    if (board->hasMoves(this->mySide)){
		// Iterates through all possible board positions:
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				Move *currMove = new Move(i, j); // Creating temporary move.
				if (board->checkMove(currMove, this->mySide)){
					allMoves[x] = currMove;
					x++;
				}
				// If currMove is not valid, then it continues to check
				//  other spaces... etc.
			}
		} // End for loop iteration.
		
		// Randomly selecting and playing a move from the array
		//  (or attempting to... -.-)
		int randMoveNum = rand() % numMoves;
		board->doMove(allMoves[randMoveNum], this->mySide);
		return allMoves[randMoveNum];
	}

	
	// Else, we must return NULL, passing our turn and allowing the
	//  opponent to play a piece:
    return NULL;
}


    
