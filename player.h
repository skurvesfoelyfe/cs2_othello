#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Side mySide; // Allowing us to call the Player Class side argument
    Side opponent;
    Board *board; // Setting up the board object.
    Move *doMove(Move *opponentsMove, int msLeft);
    
    
};

#endif
